/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokopedia;
//import java.util.*;
/**
 *
 * @author Acer
 */
public class Barang {
    private static String jenisBarang1 = "Elektronik";
    private static String namaBarang1 = "Mesin cuci Thosiba";
    private static String hargaBarang1 = "Rp. 1.500.000";
    private static String jenisBarang2 = "Gadget";
    private static String namaBarang2 = "Asus Zenphone";
    private static String hargaBarang2 = "Rp. 1.100.000";
    private static String jenisBarang3 = "Sparepart";
    private static String namaBarang3 = "Handgrip";
    private static String hargaBarang3 = "Rp. 25.000";
    
    public String getJenisBarang1() {
        return jenisBarang1;
    }

    public void setJenisBarang1(String jenisBarang1) {
        Barang.jenisBarang1 = jenisBarang1;
    }

    public String getNamaBarang1() {
        return namaBarang1;
    }

    public void setNamaBarang1(String namaBarang1) {
        Barang.namaBarang1 = namaBarang1;
    }

    public String getHargaBarang1() {
        return hargaBarang1;
    }

    public void setHargaBarang1(String hargaBarang1) {
        Barang.hargaBarang1 = hargaBarang1;
    }

    public String getJenisBarang2() {
        return jenisBarang2;
    }

    public void setJenisBarang2(String jenisBarang2) {
        Barang.jenisBarang2 = jenisBarang2;
    }

    public String getNamaBarang2() {
        return namaBarang2;
    }

    public void setNamaBarang2(String namaBarang2) {
        Barang.namaBarang2 = namaBarang2;
    }

    public String getHargaBarang2() {
        return hargaBarang2;
    }

    public void setHargaBarang2(String hargaBarang2) {
        Barang.hargaBarang2 = hargaBarang2;
    }

    public String getJenisBarang3() {
        return jenisBarang3;
    }

    public void setJenisBarang3(String jenisBarang3) {
        Barang.jenisBarang3 = jenisBarang3;
    }

    public String getNamaBarang3() {
        return namaBarang3;
    }

    public void setNamaBarang3(String namaBarang3) {
        Barang.namaBarang3 = namaBarang3;
    }

    public String getHargaBarang3() {
        return hargaBarang3;
    }

    public void setHargaBarang3(String hargaBarang3) {
        Barang.hargaBarang3 = hargaBarang3;
    }
    
    public void barang(){
        System.out.println("Daftar Barang \n");
        System.out.println("Barang 1");
        System.out.println("Jenis : " + jenisBarang1);
        System.out.println("Nama  : " + namaBarang1);
        System.out.println("harga : " + hargaBarang1);
        System.out.println("\nBarang 2");
        System.out.println("Jenis : " + jenisBarang1);
        System.out.println("Nama  : " + namaBarang1);
        System.out.println("harga : " + hargaBarang1);
        System.out.println("\nBarang 3");
        System.out.println("Jenis : " + jenisBarang1);
        System.out.println("Nama  : " + namaBarang1);
        System.out.println("harga : " + hargaBarang1);
        
    }
    
}
